package Practice;

import java.util.Stack;

public class ParenthesisChecker {

    String[] brackets = {"{}", "[]", "()"};

    public static void main(String[] args) {

        ParenthesisChecker pc = new ParenthesisChecker();
        System.out.println(""+ pc.checkParenthesis("[(])")); 

    }

    public String checkParenthesis(String str) {
        String ret = "";
        Stack<Character> stack = new Stack();

        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            
            if (ch == '{' || ch == '(' || ch == '[') {
                stack.push(ch); //push the character in the stack
                System.out.println(""+ch);
            } 
            
            if (ch == '}' || ch == ')' || ch == ']') {
                String br = stack.pop() + "" + ch;
                System.out.println(""+br);
                if(!(this.CheckBracketsPair(br))){ 
                    break;
                }
            } 
        }
        
        if(stack.isEmpty()){
            ret = "Balanced!";
        } else {
            ret = "Not Balanced!";
        }

        return ret;

    }

    public Boolean CheckBracketsPair(String br) {
        boolean ret = false;
        for (String ss : brackets) {
            if (br.equals(ss)) {
                ret = true;
            }
        }
        return ret;
    }

}
